def alg(start, end):
  l = 1
  e = 0.01
  # Словарь чисел фибоначчи
  fibs = {0: 1, 1: 1}

  # Задать начальный интервал неопределенности
  a = {0: start}
  b = {0: end}
  y = {}
  z = {}

  # Нати количество N вычислений функции как наименьшее целое

  power = b[0] - a[0]
  startInterval = power / l

  # Нахождение чисел фибоначчи, пока последнее меньше L0/l
  fibX = 1
  fibY = 1
  fN = 1
  ans = 1
  while ans < startInterval:
    fN += 1
    ans = fibX + fibY
    fibX = fibY
    fibY = ans
    fibs[fN] = ans

  k = 0
  y[k] = a[0] + (fibs[fN - 2] / fibs[fN]) * (b[0] - a[0])
  z[k] = (a[0] + (fibs[fN - 1] / fibs[fN]) * (b[0] - a[0]))

  while True:
    funcYk = func(y[k])
    funcZk = func(z[k])

    if funcYk <= funcZk:
      a[k + 1] = a[k]
      b[k + 1] = z[k]
      z[k + 1] = y[k]
      y[k + 1] = a[k + 1] + (fibs[fN - k - 3] / fibs[fN - k - 1]) * (b[k + 1] - a[k + 1])
    else:
      a[k + 1] = y[k]
      b[k + 1] = b[k]
      y[k + 1] = z[k]
      z[k + 1] = a[k + 1] + (fibs[fN - k - 2] / fibs[fN - k - 1]) * (b[k + 1] - a[k + 1])
    if k < fN - 3:
      k = k + 1
    elif k == (fN - 3):
      y[fN - 2] = z[fN - 2] = ((a[fN - 2] + b[fN - 2]) / 2)
      y[fN - 1] = y[fN - 2] = z[fN - 2]
      z[fN - 1] = y[fN - 1] + e

      if func(y[fN - 1]) <= func(z[fN - 1]):
        a[fN - 1] = a[fN - 2]
        b[fN - 1] = z[fN - 1]
      else:
        a[fN - 1] = y[fN - 1]
        b[fN - 1] = b[fN - 2]
      return ((a[fN - 1] + b[fN - 1]) / 2)


def func(x):
  return (x ** 2) - (6 * x) + 14


result = alg(-6, 6)
print("Результат: ", result)
